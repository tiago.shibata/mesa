#ifndef PEEPHOLE_H
#define PEEPHOLE_H

#include "sfn_shader.h"

namespace r600 {

bool peephole(Shader& sh);

}


#endif // PEEPHOLE_H
